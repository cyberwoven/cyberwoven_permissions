<?php

namespace Drupal\cyberwoven_permissions;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides dynamic permissions for the block content permissions module.
 */
class Permissions {

  use StringTranslationTrait;

  /**
   * Gets permissions.
   *
   * @return array
   *   Array of permissions.
   */
  public function get() {
    return $this->buildPermissions();
  }

  /**
   * @return array[]
   */
  protected function buildPermissions() {
    return [
      "administer basic site settings" => [
        'title' => $this->t('Administer %title', [
          '%title' => 'Basic site settings'
        ]),
      ],
    ];
  }

}
