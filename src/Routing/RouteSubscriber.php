<?php

namespace Drupal\cyberwoven_permissions\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $route = $collection->get('system.site_information_settings');
    if ($route) {
      $route->setRequirements([
        '_cyberwoven_permissions_access_check' => 'TRUE',
      ]);
    }
  }

}
