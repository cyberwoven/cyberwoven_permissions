<?php

namespace Drupal\cyberwoven_permissions\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Determines access to for block add pages.
 */
class CyberwovenPermissionsAccessCheck implements AccessInterface {

  /**
   * Checks access to the block add page for the block type.
   * @param \Drupal\Core\Session\AccountInterface $account
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultReasonInterface
   */
  public function access(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, "administer basic site settings");
  }

  /**
   * Example of using variables in the permission:
   * public function access($argument, AccountInterface $account) {
   *   return AccessResult::allowedIfHasPermission($account, "access $argument permission");
   * }
   */
}
